<?php
require_once 'model/wdc_dogouting_test_db.php';
require 'model/model.php';
date_default_timezone_set('America/New_York');

// get action, make it home by default
$action = filter_input(INPUT_GET, 'action');
if (!$action)
$action = filter_input(INPUT_POST, 'action');
if (!$action)
$action = 'home';

// header
include 'view/header.php';

//
/////////////////////
//outings
/////////////////////

// set up outing
if ($action == 'set_up_outing'){
	$dog_id = $_POST['dog_id'];
	$dog_name = get_dog($dog_id)['name'];
	include 'view/outing_setup.php';
}

// start outing
if ($action == 'Start Outing'){
	$dog_id = $_POST['dog_id'];
	$reason = $_POST['reason'];
	$date = $_POST['date'];
		$hour = $_POST['hour'];
		$minute = $_POST['minute'];
		$ampm = $_POST['ampm'];
		$time = $hour . ':' . $minute . ' ' . $ampm;
		$datetime = strtotime($date . $time);
	$walker_name = $_POST['walker_name'];
	start_outing($dog_id, $reason, $walker_name, $datetime);
	
	$dog_name = get_dog($dog_id)['name'];
	$action = 'home';
}

// set up end outing
if ($action == 'set_up_end_outing'){
	$dog_id = $_POST['dog_id'];
	$dog_name = get_dog($dog_id)['name'];
	$outing_id = $_POST['outing_id'];
	$time = last_out($dog_id);
	include 'view/outing_end.php';
}

// end outing
if ($action == 'End Outing'){
	$outing_id = $_POST['outing_id'];
	$dog_id = $_POST['dog_id'];
	if (isset($_POST['defecated']))
		$defecated = 1;
	else $defecated = 0;
	if (isset($_POST['urinated']))
		$urinated = 1;
	else $urinated = 0;
	$poop_score = $_POST['poop_score'];
	end_outing($outing_id, $dog_id, $defecated, $urinated, $poop_score);
	$action = 'home';
} 

/////////////////////
//

if ($action == 'edit_notes'){
	$dog_id = $_POST['dog_id'];
	$dog = get_dog($dog_id);
	$name = $dog['name'];
	$notes = $dog['notes'];
	include 'view/edit_notes.php';
}

if ($action == 'edit_notes_done'){
	$dog_id = $_POST['dog_id'];
	$notes = $_POST['notes'];
	set_dog_notes($dog_id, $notes);
	$action = 'view_dog';
}

// view dog
if ($action == 'view_dog'){
	$dog_id = filter_input(INPUT_GET, 'dog_id');
	if (!$dog_id)
		$dog_id = filter_input(INPUT_POST, 'dog_id');
	$dog = get_dog($dog_id);
	$name = $dog['name'];
	$notes = $dog['notes'];
	$status = $dog['status'];
	$inouttime;
	if ($status = "IN")
	{
		$inouttime = get_latest_outing($dog_id)['end'];
		$inouttime = date_format(date_create($inouttime), 'g:i a');
	} else
	{
		$inouttime = get_latest_outing($dog_id)['start'];
		$inouttime = date_format(date_create($inouttime), 'g:i a');
	}
	include 'view/view_dog.php';
}

// add dog
if ($action == 'Add Dog'){
	$name = $_POST['name'];
	if (is_unique($name, 'dogs', 'name')){
		add_dog($name);
	} else {
		$message = 'Please enter a unique dog name.';
	}
	$action = 'home';
}

// sign in/out
if ($action == 'sign_in_out'){
	$home_dogs = get_home_dogs();
	$wdc_dogs = get_wdc_dogs();
	include 'view/sign_in_out.php';
}

// sign in
if ($action == 'sign_in'){
	$dog_id = $_POST['dog_id'];
	set_dog_status($dog_id, 'IN');
	$outing_id = get_latest_outing($dog_id)['id'];
	end_outing($outing_id, $dog_id,0,0,0);
	$action = 'home';
}

// sign out
if ($action == 'sign_out'){
	$dog_id = $_POST['dog_id'];
	start_outing($dog_id, 'went home', '', 'now');
	set_dog_status($dog_id, 'HOME');
	$action = 'home';
}

// home (default)
if ($action == 'home'){
	$in_dogs = get_in_dogs();
	$out_dogs = get_out_dogs();
	$home_dogs = get_home_dogs();
	include 'view/home.php';
}

//footer
include 'view/footer.php';