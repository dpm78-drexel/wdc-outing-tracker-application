<body>

<!--
<a href = 'index.php?action=sign_in_out'><h3>Foster Sign In or Out</h3></a>
-->
<?php if (date('H') <= 8): ?>
	<table>
	<tr><th>Signed Out</th><th></th></tr>
	<tr><th>Dog</th><th>Sign In</th></tr>
	<?php foreach ($home_dogs as $dog): ?>	
		<tr>
			<td><a href = <?php echo 'index.php?action=view_dog&dog_id='.$dog['id']?>><?php echo $dog['name'];?></a></td>
			<td><form action='index.php' method='post'>
				<input type = 'hidden' name = 'action' value = 'sign_in'/>
				<input type = 'hidden' name = 'dog_id' value = <?php echo $dog['id'];?>/>
				<div id='buttons'><input type='submit' value='Sign In' /></div>
			</form></td>
		</tr>
	<?php endforeach;?>
	</table>
	<br/>
<?php endif; ?>

<table>
<tr><th>OUT of Kennel</th><th></th><th></th><th></th><th></th></tr>
<tr><th>Dog</th><th>Out For</th><th>Walker</th><th>Out Since</th><th>Come In</th></tr>
<?php foreach ($out_dogs as $dog): ?>	
	<tr>			
		<td><a href = <?php echo 'index.php?action=view_dog&dog_id='.$dog['dog_id']?>><?php echo $dog['name'];?></a></td>
		<td><?php echo $dog['reason']?></td>
		<td><?php echo $dog['walker_name']?></td> 
		<td><?php echo date_format(date_create($dog['start']), 'h:i a');?></td>
		<td><form action='index.php' method='post'>
			<input type = 'hidden' name = 'action' value = 'set_up_end_outing'/>
			<input type = 'hidden' name = 'dog_id' value = <?php echo $dog['dog_id'];?>/>
			<input type = 'hidden' name = 'outing_id' value = <?php echo $dog['outing_id'];?>/>
			<div id='buttons'><input type='submit' value='&#127968;' /></div>
		</form></td>
	</tr>
<?php endforeach;?>
</table>
<br/>

<table>
<tr><th>IN Kennel</th><th></th><th></th><th></th><th></th><th></th></tr>
<tr><th>Dog</th><th>In Since</th><th>Go Out</th><th>Leave WDC</th><th>Last Poop</th><th>Last Pee</th></tr>
<?php foreach ($in_dogs as $dog): ?>
	<tr>
		<td><a href = <?php echo 'index.php?action=view_dog&dog_id='.$dog['dog_id']?>><?php echo $dog['name'];?></a></td>
		<td><?php echo date_format(date_create($dog['end']), 'h:i a');?></td>
		<td><form action='index.php' method='post'>
			<input type = 'hidden' name = 'action' value = 'set_up_outing'/>
			<input type = 'hidden' name = 'dog_id' value = <?php echo $dog['dog_id'];?>/>
			<div id='buttons'><input type='submit' value='&#128694;&#128021;' /></div>
		</form></td>
		<td><form action='index.php' method='post'>
				<input type = 'hidden' name = 'action' value = 'sign_out'/>
				<input type = 'hidden' name = 'dog_id' value = <?php echo $dog['dog_id'];?>/>
				<div id='buttons'><input type='submit' value='Sign Out' /></div>
		</form></td>
		<td><?php echo get_last_poop($dog['dog_id']);?></td>
		<td><?php echo get_last_pee($dog['dog_id']);?></td>
	</tr>
<?php endforeach;?>
</table>
<br/>

<?php if (date('H') > 8): ?>
	<table>
	<tr><th>Signed Out</th><th></th></tr>
	<tr><th>Dog</th><th>Sign In</th></tr>
	<?php foreach ($home_dogs as $dog): ?>	
		<tr>
			<td><a href = <?php echo 'index.php?action=view_dog&dog_id='.$dog['id']?>><?php echo $dog['name'];?></a></td>
			<td><form action='index.php' method='post'>
				<input type = 'hidden' name = 'action' value = 'sign_in'/>
				<input type = 'hidden' name = 'dog_id' value = <?php echo $dog['id'];?>/>
				<div id='buttons'><input type='submit' value='Sign In' /></div>
			</form></td>
		</tr>
	<?php endforeach;?>
	</table>
<?php endif; ?>

<br/>
<form action='index.php' method='post'>
<input type='submit' name = 'action' value='Add Dog' /> <br>
Name: <input type='text' name='name' autofocus/>

</body>