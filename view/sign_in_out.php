<body>
<h1>Dog Sign In/Out</h1>
	<table>
	<tr><th>Signed In</th><th></th></tr>
	<tr><th>Name</th><th>Sign In</th></tr>
	<?php foreach ($home_dogs as $dog): ?>	
		<tr>
			<td><a href = <?php echo 'index.php?action=view_dog&dog_id='.$dog['id']?>><?php echo $dog['name'];?></a></td>
			<td><form action='index.php' method='post'>
				<input type = 'hidden' name = 'action' value = 'sign_in'/>
				<input type = 'hidden' name = 'dog_id' value = <?php echo $dog['id'];?>/>
				<div id='buttons'><input type='submit' value='&#128694;&#128021;' /></div>
			</form></td>
		</tr>
	<?php endforeach;?>
	</table>

	<br/>

	<table>
	<tr><th>Signed Out</th><th></th></tr>
	<tr><th>Name</th><th>Sign Out</th></tr>
	<?php foreach ($wdc_dogs as $dog): ?>	
		<tr>
			<td><a href = <?php echo 'index.php?action=view_dog&dog_id='.$dog['id']?>><?php echo $dog['name'];?></a></td>
			<td><form action='index.php' method='post'>
				<input type = 'hidden' name = 'action' value = 'sign_out'/>
				<input type = 'hidden' name = 'dog_id' value = <?php echo $dog['id'];?>/>
				<div id='buttons'><input type='submit' value='&#128694;&#128021;' /></div>
			</form></td>
		</tr>
	<?php endforeach;?>
	</table>
</form>
</body>