-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2017 at 08:52 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wdc_dogouting_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `dogs`
--

CREATE TABLE `dogs` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `status` enum('IN','OUT','HOME') NOT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dogs`
--

INSERT INTO `dogs` (`id`, `name`, `status`, `notes`) VALUES
(65, 'McBaine', 'IN', ''),
(153, 'Jerry', 'OUT', ''),
(260, 'Ffoster', 'IN', ''),
(401, 'Max', 'IN', '');

-- --------------------------------------------------------

--
-- Table structure for table `outings`
--

CREATE TABLE `outings` (
  `id` int(11) NOT NULL,
  `dog_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `reason` text NOT NULL,
  `poop_score` int(11) NOT NULL,
  `walker_name` varchar(100) NOT NULL,
  `defecated` tinyint(1) NOT NULL,
  `urinated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outings`
--

INSERT INTO `outings` (`id`, `dog_id`, `start`, `end`, `reason`, `poop_score`, `walker_name`, `defecated`, `urinated`) VALUES
(28, 153, '2017-01-18 14:17:59', '2017-01-18 14:18:49', 'another one', 0, 'lorenzo', 0, 1),
(192, 153, '2017-01-18 14:17:31', '2017-01-18 14:17:42', 'test', 2, 'Daniel Testman', 1, 0),
(429, 153, '2017-01-18 14:19:19', '0000-00-00 00:00:00', 'qw', 0, 'qw', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dogs`
--
ALTER TABLE `dogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outings`
--
ALTER TABLE `outings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dog_id` (`dog_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `outings`
--
ALTER TABLE `outings`
  ADD CONSTRAINT `outings_ibfk_1` FOREIGN KEY (`dog_id`) REFERENCES `dogs` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
