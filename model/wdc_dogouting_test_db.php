<?php
$dsn = 'mysql:host=localhost;dbname=wdc_dogouting_test';
$username = 'root';
$password = '';

try {
	$db = new PDO($dsn, $username, $password);
	//echo '<p>Connected to the database!</p>';
} catch (PDOException $e) {
	$error_message = $e->getMessage();
	echo "<p>An error occurred while connecting to
	localhost: $error_message </p>";
	exit();
}