<?php
//
/////////////////////
//general functions
/////////////////////

function is_unique($value, $table, $column){
	global $db;
	$query = 'SELECT * FROM '.$table.' WHERE '.$column.' = :value';
	$statement = $db->prepare($query);
	$statement->bindValue(':value', $value);
	$statement->execute();
	$result = $statement->fetch();
	return $result == "";
}

function get_dogs() {
	global $db;
	$query = 'SELECT * FROM dogs';
	$statement = $db->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$statement->closeCursor();
	return $result;
}

function get_dog($dog_id) {
	global $db;
	$query = 'SELECT * FROM dogs WHERE id = :dog_id';
	$statement = $db->prepare($query);
	$statement->bindValue(":dog_id", $dog_id);
	$statement->execute();
	$result = $statement->fetch();
	$statement->closeCursor();
	return $result;
}

function set_dog_status($id, $status){
	global $db;
	$query = 'UPDATE dogs SET status = :status WHERE id = :id';
	$statement = $db->prepare($query);
	$statement->bindValue(':id', $id);
	$statement->bindValue(':status', $status);
	$statement->execute();
	$statement->closeCursor();
}

function get_dog_status($id){
	global $db;
	$query = 'SELECT status FROM dogs WHERE id = :id';
	$statement = $db->prepare($query);
	$statement->bindValue(':id', $id);
	$statement->execute();
	$is_in = $statement->fetch();
	$statement->closeCursor();
	return $status;
}

function set_dog_notes($id, $notes){
	global $db;
	$query = 'UPDATE dogs SET notes = :notes WHERE id = :id';
	$statement = $db->prepare($query);
	$statement->bindValue(':id', $id);
	$statement->bindValue(':notes', $notes);
	$statement->execute();
	$statement->closeCursor();
}

function last_out($dog_id){
	global $db;
	$query = 'SELECT outings.start FROM outings WHERE dog_id = :dog_id ORDER BY start DESC LIMIT 1';
	$statement = $db->prepare($query);
	$statement->bindValue(':dog_id', $dog_id);
	$statement->execute();
	$result = $statement->fetch();
	$statement->closeCursor();
	$last_out = date_create($result[0]);
	return date_format($last_out, 'g:i a');
}

function get_last_poop($dog_id){
	global $db;
	$query = 'SELECT outings.end FROM outings WHERE dog_id = :dog_id AND defecated = 1 AND end > CURDATE() ORDER BY end DESC LIMIT 1';
	$statement = $db->prepare($query);
	$statement->bindValue(':dog_id', $dog_id);
	$statement->execute();
	$result = $statement->fetch();
	$statement->closeCursor();
	if ($result[0] == 0)
		$last_poop = "Not yet today!";
	else
		$last_poop = date_format(date_create($result[0]), 'g:i a');
	return $last_poop;
}

function get_last_pee($dog_id){
	global $db;
	$query = 'SELECT end FROM outings WHERE dog_id = :dog_id AND urinated = 1 AND end > CURDATE() ORDER BY end DESC LIMIT 1';
	$statement = $db->prepare($query);
	$statement->bindValue(':dog_id', $dog_id);
	$statement->execute();
	$result = $statement->fetch();
	$statement->closeCursor();
	if ($result[0] == 0)
		$last_pee = "Not yet today!";
	else
		$last_pee = date_format(date_create($result[0]), 'g:i a');;
	return $last_pee;
}

function get_latest_outing($dog_id){
	global $db;
	$query = 'SELECT * FROM outings WHERE dog_id = :dog_id ORDER BY start DESC LIMIT 1';
	$statement = $db->prepare($query);
	$statement->bindValue(':dog_id', $dog_id);
	$statement->execute();
	$outing = $statement->fetch();
	$statement->closeCursor();
	return $outing;
}

function start_outing($dog_id, $reason, $walker_name, $time) {
	global $db;
	$query = 'INSERT INTO outings (id, dog_id, start, reason, walker_name) VALUES (:id, :dog_id, :time, :reason, :walker_name)';
	$statement = $db->prepare($query);

	//unique id
	do{
		$id = rand(0,500);
	}while (!is_unique($id, 'outings', 'id'));

	//format time
	if ($time == 'now')
		$time = date('YmdHis');
	else
		$time = date('YmdHis', $time);
	$statement->bindValue(':time', $time);

	//everything else
	$statement->bindValue(':id', $id);
	$statement->bindValue(':dog_id', $dog_id);
	$statement->bindValue(':reason', $reason);
	$statement->bindValue(':walker_name', $walker_name);
	$statement->execute();
	$statement->closeCursor();
	set_dog_status($dog_id, 'OUT');
	return $id;
}

function end_outing($outing_id, $dog_id, $defecated, $urinated, $poop_score) {
	global $db;
	$query = 'UPDATE outings SET end = :time, defecated = :defecated, urinated = :urinated, poop_score = :poop_score WHERE id = :outing_id';
	$statement = $db->prepare($query);

	//current time
	$time = date("YmdHis");
	$statement->bindValue(':time', $time);

	//everything else
	$statement->bindValue(':outing_id', $outing_id);
	$statement->bindValue(':defecated', $defecated);
	$statement->bindValue(':urinated', $urinated);
	$statement->bindValue(':poop_score', $poop_score);
	$statement->execute();
	$statement->closeCursor();
	set_dog_status($dog_id, 1);
}

function get_in_dogs(){
	global $db;
	$query = '
		SELECT *, dogs.id AS "dog_id" FROM dogs
		LEFT JOIN
		(
			SELECT *, id AS "outing_id" FROM outings o1
			JOIN
			(
				SELECT max(start) max_start, dog_id dog_id2 FROM outings
				GROUP BY dog_id
			) o2
			WHERE o1.start = o2.max_start
			AND o1.dog_id = o2.dog_id2
		) outings
		ON dogs.id = outings.dog_id
		WHERE dogs.status = "IN"
		ORDER BY outings.end DESC
	';
	$statement = $db->prepare($query);
	$statement->execute();
	$dogs = $statement->fetchAll();
	$statement->closeCursor();
	return $dogs;
}

function get_out_dogs(){
	global $db;
	$query = '
		SELECT *, dogs.id AS "dog_id" FROM dogs
		LEFT JOIN
		(
			SELECT *, id AS "outing_id" FROM outings o1
			JOIN
			(
				SELECT max(start) max_start, dog_id dog_id2 FROM outings
				GROUP BY dog_id
			) o2
			WHERE o1.start = o2.max_start
			AND o1.dog_id = o2.dog_id2
		) outings
		ON dogs.id = outings.dog_id
		WHERE dogs.status = "OUT"
		ORDER BY outings.start DESC
	';
	$statement = $db->prepare($query);
	$statement->execute();
	$dogs = $statement->fetchAll();
	$statement->closeCursor();
	return $dogs;
}

function get_home_dogs(){
	global $db;
	$query = 'SELECT * FROM dogs WHERE status = "HOME" ORDER BY name';
	$statement = $db->prepare($query);
	$statement->execute();
	$dogs = $statement->fetchAll();
	$statement->closeCursor();
	return $dogs;
}

function add_dog($name) {
	global $db;
	$query = 'INSERT INTO dogs (id, name, status) VALUES (:id, :name, 0)';
	$statement = $db->prepare($query);
	do{
		$id = rand(0,500);
	}while (!is_unique($id, 'dogs', 'id'));	

	$statement->bindValue(':id', $id);
	$statement->bindValue(':name', $name);
	$statement->execute();
	$statement->closeCursor();

	$setup_outing_id = start_outing($id, 'added to db', 'admin', date('now'));
	end_outing($setup_outing_id, $id, 0, 0, 0);
}